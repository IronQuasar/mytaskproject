﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.DTO
{
    public class TaskDTO
    {
        public Guid Id { get; set; }
        public int Number { get; set; }
        public string NameTask { get; set; }
        public string Description { get; set; }
        public string ApplicationUserId { get; set; }
    }
}
