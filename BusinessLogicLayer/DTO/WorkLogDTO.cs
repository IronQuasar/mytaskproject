﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.DTO
{
    public class WorkLogDTO
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public string Time { get; set; }
        public string Description { get; set; }
        public string ApplicationUserId { get; set; }
        public Guid TaskId { get; set; }
    }
}
