﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.DTO
{
    public class TaskViewModel
    {
        public string NameTask { get; set; }
        public string Description { get; set; }

        public string ApplicationUserId { get; set; }
    }
}
