﻿using AutoMapper;
using BusinessLogicLayer.DTO;
using BusinessLogicLayer.Model;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Services
{
    public class MappingProfile:Profile
    {
        public MappingProfile()
        {
            CreateMap<TaskViewModel,TaskDTO>();
            CreateMap<IntermediateWorkLog, WorkLogDTO>();
            CreateMap<Task, TaskDTO>();
            CreateMap<WorkLog, TaskWorkLog>()
                .ForMember(dest => dest.TaskName,src=>src.MapFrom(val=>val.Task.NameTask))
                .ForMember(dest=>dest.UserName,src=>src.MapFrom(val=>val.ApplicationUser.UserName))
                .ForMember(dest=>dest.Time,src=>src.MapFrom(val=>val.Time))
                .ForMember(dest=>dest.Date,src=>src.MapFrom(val=>val.Date));
                
        }
    }
}
