﻿using AutoMapper;
using BusinessLogicLayer.DTO;
using BusinessLogicLayer.Interface;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Services
{
    public class TaskService:ITaskService
    {
        ITaskRepository Database { get; set; }
        IMapper _mapper;
        public TaskService(ITaskRepository uof, IMapper mapper)
        {
            Database = uof;
            _mapper = mapper;
        }
        public void CreateTask(TaskDTO taskDto)
        {
            Task task = new Task
            {

                ApplicationUserId = taskDto.ApplicationUserId,
                NameTask = taskDto.NameTask,
                Description = taskDto.Description

            };
            Database.Create(task);
        }
        public IEnumerable<TaskDTO> GetTasks()
        {

            return _mapper.Map<IEnumerable<Task>, List<TaskDTO>>(Database.GetAll());
        }
        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
