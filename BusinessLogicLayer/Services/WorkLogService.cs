﻿using AutoMapper;
using BusinessLogicLayer.DTO;
using BusinessLogicLayer.Interface;
using BusinessLogicLayer.Model;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer.Services
{
    public class WorkLogService : IWorkLogService
    {
        IWorkLogRepository Database { get; set; }
        IMapper _mapper;
        public WorkLogService(IWorkLogRepository uof,IMapper mapper)
        {
            Database = uof;
            _mapper = mapper;
        }
        public void CreateWorkLog(WorkLogDTO workLogDTO)
        {
            WorkLog workLog = new WorkLog
            {
 
                Description = workLogDTO.Description,
                TaskId=workLogDTO.TaskId,
                ApplicationUserId =workLogDTO.ApplicationUserId,
                Time = Convert.ToInt32(workLogDTO.Time),
                Date = workLogDTO.Date
                
            };
            Database.Create(workLog);
        }
        public IEnumerable<WorkLogDTO> GetWorkLogs()
        {
            var workLogs = Database.GetAll();
            return _mapper.Map<IEnumerable<WorkLog>, List<WorkLogDTO>>(workLogs);
        }

        public IEnumerable<TaskWorkLog> GetWorkLogList(DateTime Date)
        {
            var workLogs = Database.GetAll().Where(dt=>dt.Date==Date);
            var result = new List<TaskWorkLog>();
            result = _mapper.Map<List<TaskWorkLog>>(workLogs);

            return result;
        }
        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
