﻿using BusinessLogicLayer.DTO;
using BusinessLogicLayer.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Interface
{
    public interface IWorkLogService
    {
        void CreateWorkLog(WorkLogDTO workLogDTO);
        IEnumerable<WorkLogDTO> GetWorkLogs();
        IEnumerable<TaskWorkLog> GetWorkLogList(DateTime Date);
        void Dispose();
    }
}
