﻿using BusinessLogicLayer.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Interface
{
    public interface ITaskService
    {
        void CreateTask(TaskDTO taskDto);
        IEnumerable<TaskDTO> GetTasks();
        void Dispose();
    }
}
