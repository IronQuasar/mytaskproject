﻿using DataAccessLayer.Config;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Repositories;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Infrastructure
{
    public class ServiceModule : NinjectModule
    {
        private string connectionString;
        public ServiceModule(string connection)
        {
            connectionString = connection;
        }
        public override void Load()
        {
            Bind<ITaskRepository>().To<TaskRepository>();
            Bind<IWorkLogRepository>().To<WorkLogRepository>();
            Bind<ConnectionStringConfig>().ToMethod(x =>
               new ConnectionStringConfig
               {
                   ConnectionString = connectionString
               });
        }
    }
}
