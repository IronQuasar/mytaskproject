﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Model
{
   public class TaskWorkLog
    {
        public string TaskName { get; set; }
        public DateTime Date { get; set; }
        public string UserName { get; set; }
        public double Time { get; set; }
    }
}
