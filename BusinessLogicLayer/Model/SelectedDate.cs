﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Model
{
   public class SelectedDate
    {
        public DateTime Date { get; set; }
    }
}
