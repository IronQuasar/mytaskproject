﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Model
{
   public class IntermediateWorkLog
    {
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public string TaskName { get; set;}
        public string Time { get; set; }
        public string ApplicationUserId { get; set; }
        public Guid TaskId { get; set; }

    }
}
