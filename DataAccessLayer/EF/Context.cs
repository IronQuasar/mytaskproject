﻿using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Proxies;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.EF
{
    public class Context : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Task> Tasks {get;set;}
        public DbSet<WorkLog> WorkLogs { get; set; }

        public Context(DbContextOptions<Context> options)
            : base(options)
        {
          
        }
    }
}
