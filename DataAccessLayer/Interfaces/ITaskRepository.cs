﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Interfaces
{
    public interface ITaskRepository: IDisposable
    {
        void Create(Task item);
        IEnumerable<Task> GetAll();
    }
}
