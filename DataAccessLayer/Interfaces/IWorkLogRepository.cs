﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Interfaces
{
    public interface IWorkLogRepository:IDisposable
    {
        void Create(WorkLog item);
        IEnumerable<WorkLog> GetAll();
    }
}
