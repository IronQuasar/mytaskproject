﻿using DataAccessLayer.Config;
using DataAccessLayer.EF;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Repositories
{
    public class TaskRepository: BaseRepository, ITaskRepository
    {

        public TaskRepository(ConnectionStringConfig config) : base(config)
        {
        }
        public void Create(Task task)
        {
            using (var context = ResolveContext())
            {
                context.Tasks.Add(task);
                context.SaveChanges();
            }
        }
        public IEnumerable<Task> GetAll()
        {
            using (var context = ResolveContext())
            {            
                return context.Tasks.ToList();
            }
        }

    }
}
