﻿using DataAccessLayer.Config;
using DataAccessLayer.EF;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Repositories
{
    public class WorkLogRepository : BaseRepository, IWorkLogRepository
    {
        public WorkLogRepository(ConnectionStringConfig config) : base(config)
        {

        }

        public void Create(WorkLog workLog)
        {
            using (var context = ResolveContext())
            {
                context.WorkLogs.Add(workLog);
                context.SaveChanges();
            }
        }
        public IEnumerable<WorkLog> GetAll()
        {
            using (var context = ResolveContext())
            {
                return context.WorkLogs
                    .Include(p => p.Task)
                    .Include(p => p.ApplicationUser)
                    .ToList();
            }
        }
       
    }
}
