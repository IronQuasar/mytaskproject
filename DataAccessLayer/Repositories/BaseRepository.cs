﻿using DataAccessLayer.Config;
using DataAccessLayer.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Protocols;
using System;
using System.Configuration;
using System.Net.NetworkInformation;

namespace DataAccessLayer.Repositories
{
    public class BaseRepository
    {
        private Context db;
        private bool disposed = false;
        private string connection;
        public BaseRepository(ConnectionStringConfig config)
        {
            connection = config.ConnectionString;
        }

        public Context ResolveContext()
        {
            var options = new DbContextOptionsBuilder<Context>();
            options.UseSqlServer(connection);
            return db = new Context(options.Options);
        }
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                this.disposed = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}