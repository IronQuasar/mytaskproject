﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public ICollection<WorkLog> WorkLogs { get; set; }
        public ICollection<Task> Tasks { get; set; }
    }
}
