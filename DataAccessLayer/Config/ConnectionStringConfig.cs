﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Config
{
    public class ConnectionStringConfig
    {
        public string ConnectionString { get; set; }
    }
}
