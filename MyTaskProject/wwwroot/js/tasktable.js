﻿var headtable = new Vue({
    el: '#tasktable',
    data: {
        todos: [
            { day: 'Mon' },
            { day: 'Tue' },
            { day: 'Wed' },
            { day: 'Thu' },
            { day: 'Fri' },
            { day: 'Sat' },
            { day: 'Sun' }
        ],
        topMessage: 'Select a Day',
        month: 0,
        selectedDate: ''
    },
    methods: {
        createMonth(month) {
            var dayCount = 0;
            var nowDate = new Date();
            var currentMonth = nowDate.getMonth();
            var currentYear = nowDate.getFullYear();
            var temp = new Date(currentYear, currentMonth + 1 + month, 0);
            var lastDay = temp.getDate();

            var week = 0;
            var currentDay = new Date(currentYear, currentMonth + month, 1);
            var t = currentDay.getDay() - 1;
            if (t < 0) { t = 6; }

            while (currentDay.getMonth() == currentMonth + month) {
                var i = 0;
                while (i < 7) {
                    var date = document.getElementsByTagName('td');
                    var currentDate = currentDay.getDate();
                    if (week == 0) {
                        if (i < t) {
                            date[dayCount].innerHTML = " ";

                        }
                        else {
                            date[dayCount].innerHTML = currentDay.getDate();
                            date[dayCount].setAttribute('data-url-date', currentDay.toDateString());
                            date[dayCount].className = 'bg-primary selected-day';
                            currentDay.setDate(currentDay.getDate() + 1);
                        }
                    } else {
                        date[dayCount].innerHTML = currentDay.getDate();
                        date[dayCount].setAttribute('data-url-date', currentDay.toDateString());
                        date[dayCount].className = 'bg-primary selected-day';                     
                        currentDay.setDate(currentDay.getDate() + 1);

                    }
                    dayCount++;
                    i++;
                    if (currentDate == lastDay) { break; }
                }
                week++;
            }
        },
        clearMonth() {
            const tables = document.getElementsByClassName('task');
            for (let table of tables) {
                const tds = table.getElementsByTagName('td');
                for (let td of tds) {
                    console.log(td);
                    td.removeAttribute('data-url-date');
                    td.className = "";
                    td.innerHTML = "";
                    console.log(td);
                }
            }
        },
        previousMonth() {
            this.clearMonth();
            this.createMonth(--this.month);
        },
        nextMonth() {
            this.clearMonth();
            this.createMonth(++this.month);
        },
        showAllWorkLogsOnThisDay(e) {
            this.selectedDate = e.target.getAttribute('data-url-date');
            document.getElementById('tasktable').style.display = 'none';
            document.getElementById('formworkloglist').style.display = 'flex';
            this.showAllWorkLogs();
            console.log(this.selectedDate);
        },
        load(url, element) {
            req = new XMLHttpRequest();
            req.open("GET", url, false);
            req.send(null);

            element.innerHTML = req.responseText;
        },
        showAllWorkLogs() {
            var url_dash = document.getElementById('formworkloglist').getAttribute('data-url-dash'); //найти решение с Load
            let selectDate = {
                Date: this.selectedDate
            }
            axios
                .post(url_dash, {
                    Date: this.selectedDate
                })
                .then(console.log(selectDate))
            console.log(selectDate);
        }

    },
    created() {
        this.createMonth(this.month);
    }

});