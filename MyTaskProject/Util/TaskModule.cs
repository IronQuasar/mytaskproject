﻿using BusinessLogicLayer.Interface;
using Ninject.Modules;
using BusinessLogicLayer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Configuration;

namespace MyTaskProject.Util
{
    public class TaskModule : NinjectModule
    {
        
        public override void Load()
        {
            Bind<ITaskService>().To<TaskService>();
        }
    }
}
