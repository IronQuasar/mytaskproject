﻿using BusinessLogicLayer.Interface;
using BusinessLogicLayer.Services;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyTaskProject.Util
{
    public class WorkLogModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IWorkLogService>().To<WorkLogService>();
        }
    }
}
