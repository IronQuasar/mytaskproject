﻿let month = 0;
//(function () {
//    //createMonth(month);
//    let selectedDate;
//    $('#date').on('input keyup', function () {
//        let time = timeValid(this.value);
//        if (time) {
//            $(".form-error").css("display", "none");
//        }
//        else {
//            $(".form-error").css("display", "block");
//        }
//    });
//    $('.task td').click(function () {
//        selectedDate = $(this).data("url-date");
//        showAllWorkLogs(selectedDate);
//        $('#tasktable').css("display","none");
//        $("#formworkloglist").css("display", "flex");
//    });
//})(jQuery);

function nextMonth() {
    clearMonth();
    createMonth(++month);
}

function previousMonth() {
    clearMonth();
    createMonth(--month);
}

function clearMonth() {
    const tables = $('.task');
    for (let table of tables) {
        const tds = $(table).find('td');
        tds.empty();
        tds.removeAttr("data-url-date");
        tds.removeClass('bg-primary selected-day');
    }
}

function createMonth(month)
{
    var dayCount = 0;
    var nowDate = new Date();
    var currentMonth = nowDate.getMonth();
    var currentYear = nowDate.getFullYear();
    var temp = new Date(currentYear, currentMonth + 1 + month, 0);
    var lastDay = temp.getDate();

    var week = 0;
    var currentDay = new Date(currentYear, currentMonth + month, 1);
    var t = currentDay.getDay() - 1;
    if (t < 0) { t = 6;}

    while (currentDay.getMonth() == currentMonth + month)
    {
        var i = 0;
        while (i < 7)
        {
            var currentDate = currentDay.getDate();
            if (week == 0) {
                if (i < t) {
                    $('.task td')[dayCount].innerHTML = " ";

                }
                else
                {
                    $('.task td')[dayCount].innerHTML = currentDay.getDate();
                    $('.task td')[dayCount].setAttribute('data-url-date', currentDay.toDateString());
                    $('.task td')[dayCount].className ='bg-primary selected-day';
                    currentDay.setDate(currentDay.getDate() + 1);
                }
            } else {
                $('.task td')[dayCount].innerHTML = currentDay.getDate();
                $('.task td')[dayCount].setAttribute('data-url-date', currentDay.toDateString());
                $('.task td')[dayCount].className = 'bg-primary selected-day';
                currentDay.setDate(currentDay.getDate() + 1);
                
            }
            dayCount++;
            i++;
            if (currentDate == lastDay) { break; }   
        }
        week++;
    }
}

function closeViewWorkLogList() {
    $('#formworkloglist').css("display", "none");
    $('#tasktable').css("display", "block");
    
}

function showFormWorkLog(selectedDate) {
    taskDropDownList();
    $('#formworkloglist').css("display", "none");
    $("#formworklog").css("display", "flex");
    $('#worklog').click(
        { selectedDate },
        function (event)
        {
            createWorkLog(event.data.selectedDate);
        });
}

function closeFormCreateWorkLog() {
    $('#tasktable').css("display", "block");
    $("#formworklog").css("display", "none");
    $('#dropdown').empty();
    $("#date").val('');
    $("#description").val('');
    $(".form-error").css("display", "none");
}

function timeValid(time) {
    if (time.match(/\d{1,10}h/) || time.match(/\d{1,10}w/) || time.match(/\d{1,10}d/) || time.match(/\d{1,10}m/)) {
        return true;
    }
    else {
        return false;
    }
}


function showAllWorkLogs(selectedDate) {
    var url_dash = $('#formworkloglist').data('url-dash');
    var selectDate = {
        Date: selectedDate
    }
    $('#formworkloglist').load(url_dash,
        {
            "Date": selectDate
        }
        ,
        function () {
        $('#addWorkLog').click(
            { selectedDate },
            function (event) {
                showFormWorkLog(event.data.selectedDate);
            });
    });
    
}

function createWorkLog(selectedDate) {
    var workLog = {
        Time: $('#date').val(),
        Description: $('#description').val(),
        TaskId: $("option:selected").data('id'),
        Date: selectedDate
    }
    var time = timeValid(workLog.Time);
    if (time) {
        var url = $('.form-container').data('url-save');
        console.log(workLog);
        $.ajax({
            type: "POST",
            url: url,
            data: JSON.stringify(workLog),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                alert(data);
            }
        });
        
        closeFormCreateWorkLog();
    }
}


function taskDropDownList() {
    var url = $('.task').data('url-get')
    $.ajax({
        type: "GET",
        url: url,
        success: function (data) {
            for (var option of data) {
                $('#dropdown').append('<option data-id=\"' + option.id + '\">' + option.nameTask + '</option>');
            }
        }
    });
};