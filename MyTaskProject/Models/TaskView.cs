﻿using BusinessLogicLayer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyTaskProject.Models
{
    public class TaskView
    {
        public string NameTask { get; set; }
        public string Description { get; set; }
        
        public string ApplicationUserId { get; set; }
    }
}
