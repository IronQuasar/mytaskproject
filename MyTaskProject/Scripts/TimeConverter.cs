﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MyTaskProject.Scripts
{
    public class TimeConverter
    {
        public int ConvertingTime(string time)
        {
            var minute = 0;
            Dictionary<string, int> timeUnit = new Dictionary<string, int>{
                {"h",60},
                {"d",480 },
                {"w",2400 },
                {"m",1}
            };
            foreach (var item in timeUnit)
            {
                var pattern = $"\\d+(?={item.Key})";
                var regex = new Regex(pattern);
                if (regex.IsMatch(time))
                {
                    var value = regex.Match(time).Value;
                    minute += Convert.ToInt32(value) * item.Value;

                }
            }
            return minute;
        }
    }
}
