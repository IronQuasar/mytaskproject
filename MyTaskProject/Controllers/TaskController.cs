﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogicLayer.DTO;
using BusinessLogicLayer.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using MyTaskProject.Models;

namespace MyTaskProject.Controllers
{
    public class TaskController : Controller
    {
        ITaskService taskService;
        IMapper _mapper;
        public TaskController(ITaskService serv, IMapper mapper)
        {
            _mapper = mapper;
            taskService = serv;
        }
        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateTask(TaskViewModel task)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            task.ApplicationUserId = userId;
            var taskDTO = _mapper.Map<TaskViewModel, TaskDTO>(task);
            taskService.CreateTask(taskDTO);

            return Redirect("~/Home/Index");
        }
        
        protected override void Dispose(bool disposing)
        {
            taskService.Dispose();
            base.Dispose(disposing);
        }
    }
}