﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogicLayer.DTO;
using BusinessLogicLayer.Interface;
using AutoMapper;
using BusinessLogicLayer.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using MyTaskProject.Models;
using Microsoft.AspNetCore.Authorization;
using BusinessLogicLayer.Model;
using System.Security.Claims;
using MyTaskProject.Scripts;


namespace MyTaskProject.Controllers
{

    public class HomeController : Controller
    {
        ITaskService taskService;
        IWorkLogService workLogService;
        IMapper _mapper;
        public HomeController(ITaskService serv, IWorkLogService work, IMapper mapper)
        {
            taskService = serv;
            workLogService = work;
            _mapper = mapper;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult About()
        {
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }
        [HttpPost]
        public IActionResult CreateWorkLog([FromBody]IntermediateWorkLog workLog)
        {
            TimeConverter minute = new TimeConverter();
            workLog.Time = Convert.ToString(minute.ConvertingTime(workLog.Time));
            workLog.ApplicationUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var WokrLogDTO = _mapper.Map<IntermediateWorkLog, WorkLogDTO>(workLog);
            workLogService.CreateWorkLog(WokrLogDTO);
            return Redirect("~/Home/DashBoard");
        }

        [Authorize]
        public IActionResult DashBoard()
        {

            return View();
        }
        [HttpGet]
        public IActionResult AllTaskView()
        {
            IEnumerable<TaskDTO> taskDTOs = taskService.GetTasks();
            return Json(taskDTOs);
        }
        [HttpPost]
        public IActionResult WorkLogList([FromBody]SelectedDate date)
        {
            IEnumerable<TaskWorkLog> workLogDTOs = workLogService.GetWorkLogList(date.Date);
            return PartialView(workLogDTOs);
        }
        
    }
}
